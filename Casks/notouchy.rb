cask "notouchy" do
  arch arm: "aarch64", intel: "x86_64"

  version "0.1.0-dev.20240127.1188"
  sha256 \
    arm: "e50b10794378dbb1992742ca3c6f563f952ab566a48cebdb72acf26fd95a2bc7",
    intel: "c3e59c0d52e362dfb4c1fc68fa558333bca555732b621cc4d9709eb77afc9384"

  url "https://gitlab.com/notouchy/releases/-/releases/" \
      "#{version}/downloads/notouchy-macos-#{arch}-#{version}.tar.gz",
    verified: "gitlab.com/notouchy/releases/"

  name "No Touchy"
  desc "Simple speech recognition"
  homepage "https://notouchy.app"

  depends_on macos: ">= :big_sur" # 11.0

  app "No Touchy.app"

  uninstall quit: "app.notouchy.NoTouchy"

  zap trash: [
    "~/Library/Application Support/No Touchy",
    "~/Library/Caches/No Touchy",
    "~/Library/Preferences/com.notouchy.No Touchy.plist",
  ]

  caveats do
    unsigned_accessibility
    license "https://notouchy.app/eula"
  end
end
