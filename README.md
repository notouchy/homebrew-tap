# homebrew-tap

The easy way to install [No Touchy](https://notouchy.app), using [Homebrew](https://docs.brew.sh).

## Install homebrew

```sh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

## Add the tap

```sh
brew tap --force-auto-update notouchy/tap https://gitlab.com/notouchy/homebrew-tap
```

## Install the app

```sh
brew install --cask --no-quarantine notouchy
```

## Upgrade

```
brew update
brew upgrade notouchy
```

or, if you don't want to run `brew update`:
```
cd $(brew --prefix)/Homebrew/Library/Taps/notouchy/homebrew-tap && git pull && cd -
brew upgrade notouchy
```

## Uninstall

```
brew uninstall --zap notouchy
brew cleanup --prune=all notouchy
```

## License

This tap, consisting of only `README.md` and `Casks/notouchy.rb` (**NOT** _No Touchy_ itself), is available under the [Apache-2 license](https://opensource.org/license/apache-2-0).
